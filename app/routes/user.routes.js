module.exports = (router) => {
	const user = require('../controllers/user.controller.js');

	router.route('/user').post(user.update);

	// get all the users
	router.route('/users').get(user.findAll);

	router.route('/users/:user_uuid').get(user.findOne);

	router.route('/lst').post(user.lst);
};