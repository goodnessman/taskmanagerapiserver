
exports.updateServices = (savesDataJson, newDataJson) => {
    let result = newDataJson;

    if (!savesDataJson) {
        return result;
    }

    try {
        let savedData = JSON.parse(savesDataJson),
        newData = JSON.parse(newDataJson);

        savedData.services.map(service => {
            if (service.name === 'horoscope') {
                let newService = newData.services.find(serviceItem => {
                    return serviceItem.name === service.name;
                });

                newService.status = service.status;
            }
        });

        result = JSON.stringify(newData);
    } catch(e) {
        return result;
    }

    return result;
};

exports.isNewDevice = (data) => {
    let result = false;

    if (!data) {
        return result;
    }

    try {
        let requestData = JSON.parse(data);

        if (!requestData.months && !requestData.years && !requestData.repeatedTasks && !requestData.categories && !requestData.services) {
            result = true;
        }
    } catch(e) {
        return result;
    }

    return result;
}