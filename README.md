Documentation: https://docs.google.com/document/d/1RqAhh4A1ABCvFJrG3B3Roh0O8k5IqP_v9JtVxOW72Vg/edit?usp=sharing

# Building a RESTful API in Node and Express

Using the new Express 4.0 Router to build an API

[Read the tutorial](http://scotch.io/tutorials/javascript/build-a-restful-api-using-node-and-express-4)

## Requirements

- Node and npm

## Installation

- Clone the repo: `git clone git@github.com:scotch-io/node-api`
- Install dependencies: `npm install`
- Start the server: `node server.js` (npm run start)
- In Browser open: `http://localhost:8080`

## Testing the API
Test your API using [Postman](https://chrome.google.com/webstore/detail/postman-rest-client-packa/fhbjgbiflinjbdggehcddcbncdddomop)


## Nikita Pikovets

Databse: https://cloud.mongodb.com/

Start: nodemon server.js

## Git

one7days_server: Version-1.1: ADD: task-49: Add admin panel
