const Horoscope = require('../models/horoscope.model.js');
const requestPromise = require('request-promise');
const parseString = require('xml2js').parseString;

exports.getForToday = (req, res) => {
	Horoscope.findOne({date: req.params.date}, function(err, todayForecast) {
		if (err) {
			res.json(getErrorResponse(err));
		}
		if (todayForecast) {
			let regex = new RegExp(getMonthDateFormat(req.params.date));

			Horoscope.find({date: regex}, function(err, monthForecast) {
				if (err) {
					res.json(getErrorResponse(err));
				}
				if (monthForecast) {
					res.json({
						message: '',
						error: false,
						data: monthForecast
					});
				}
			});
		} else {
			requestPromise('http://img.ignio.com/r/export/utf/xml/daily/com.xml').then(body => {
				parseString(body, function (err, result) {
					if (err) {
						res.json(getErrorResponse(err));
					} else {
						let parsedForecast = parseHoroscopeRespoince(result);

						res.json({
							message: '',
							error: false,
							data: parsedForecast
						});
					}
				});
				}).catch(err => {
					res.json(getErrorResponse(err));
			});
		}
	});
};

exports.receive = (req, res) => {
	requestPromise('http://img.ignio.com/r/export/utf/xml/daily/com.xml').then(body => {
	  parseString(body, function (err, result) {
		if (err) {
		  res.json({
			message: err,
			error: true,
			data: null
		  });
		} else {
			let parsedForecast = parseHoroscopeRespoince(result);

			res.json({
				message: '',
				error: false,
				data: parsedForecast
			});
		}
	  });
	}).catch(err => {
	  res.json({
		message: '',
		error: true,
		data: err
	  });
	});
};

function parseHoroscopeRespoince(horoscopeResponce) {
	if (!horoscopeResponce || horoscopeResponce.error) {
	  console.log('Bad horoscope service responce');
	  return false;
	}
	let datesObj = horoscopeResponce.horo.date[0]['$'];
	let datesArr = Object.keys(datesObj),
	  parsedHoroscope = [];

	  datesArr.map((date) => {
	  if (date !== 'yesterday') {
		let parsedHoroscopeItem = {};

		parsedHoroscopeItem.date = horoscopeResponce.horo.date[0]['$'][date];
		parsedHoroscopeItem.signs = getGoroscopeSigns(date, horoscopeResponce.horo);
		parsedHoroscope.push(parsedHoroscopeItem);

		let horoscope = new Horoscope(parsedHoroscopeItem);
		horoscope.save();
	  }
	});

	return parsedHoroscope;
}

function getGoroscopeSigns(date, data) {
	if (!date || !data) {
	  console.log('Bad function params');
	  return false;
	}
	let signs = Object.keys(data),
	  parsedSigns = [];

	signs.map((sign) => {
	  if (sign !== 'date') {
		let parsedSignsItem = {};

		parsedSignsItem.sign = sign;
		parsedSignsItem.forecast = data[sign][0][date][0];
		parsedSigns.push(parsedSignsItem);
	  }
	});

	return parsedSigns;
}

function getMonthDateFormat(queryString) {
	if (!queryString) {
		return '';
	}

	let dateArr = queryString.split('.');

	dateArr.shift();

	return dateArr.join('.');

}

function getErrorResponse(err) {
	return {
		message: err ? err.message : '',
		error: true,
		data: null
	}
}