module.exports = (router) => {
	const horoscope = require('../controllers/horoscope.controller.js');

	router.route('/horoscope').get(horoscope.receive);

	router.route('/horoscope/:date').get(horoscope.getForToday);
};