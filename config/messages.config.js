module.exports = {
  serverHasConnected: 'Application connected to server',
  dataSavedSuccess: 'Data saved successfully',
  notValidData: 'Not valid data sent!',
  savingError: 'Error save data!',
  connectError: 'Error connect to server!',
  getTasksError: 'Error load tasks fromserver!',
  getDataSuccess: 'Data successfully updated',
  userNotFound: 'User with this uuid not found!',
  welcome: 'Welcome to xProject application.',
  statusOk: 'OK'
};