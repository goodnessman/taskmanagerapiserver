const User = require('../models/user.model.js');
const messagesConfig = require('../../config/messages.config.js');
const userUtil = require('../utils/user.util.js');

exports.lst = (req, res) => {
	res.json({
		message: '',
		error: false,
		data: req.body
	});
};

exports.findOne = (req, res) => {
	User.findOne({uuid:req.params.user_uuid}, function(err, user) {
		if (err) {
			res.json({
				message:messagesConfig.getTasksError,
				error: true,
				data: err
			});
		}
		if (user) {
			res.json({
				message:messagesConfig.getDataSuccess,
				error: false,
				data: user
			});
		}else {
			res.json({
				message:messagesConfig.userNotFound,
				error: true,
				data: null
			});
		}
	});
};

exports.findAll = (req, res) => {
	User.find(function(err, users) {
		if (err) {
			res.json({
				message: messagesConfig.connectError,
				error: true,
				data: err
			});
		}
		res.json({
			message: messagesConfig.statusOk,
			error: false,
			data: users
		});
	});
};


exports.update = (req, res) => {
	// create a new instance of the User model
	const user = new User({
		tasks: req.body.tasks,
		uuid: req.body.uuid
	});

	if (!req.body.tasks && !req.body.uuid) {
		res.json({
			message: messagesConfig.notValidData,
			error: true,
			data: null
		});
	}
	User.findOne({uuid:req.body.uuid}, function(err, result) {
		if (err) {
			res.json({
				message: messagesConfig.savingError,
				error: true,
				data: err
			});
		}
		if (result) {
			User.findById(result._id, function(err, userResult) {
				if (err) {
					res.json({
						message: messagesConfig.savingError,
						error: true,
						data: err
					});
				}
				if (userResult.tasks === req.body.tasks) {
					res.json({
						message: messagesConfig.dataSavedSuccess,
						error: false,
						data: userResult
					});
					return;
				}
				if (userUtil.isNewDevice(req.body.tasks)) {
					res.json({
						message: messagesConfig.dataSavedSuccess,
						data: userResult
					});
					return;
				}

				let data = userUtil.updateServices(userResult.tasks, req.body.tasks);

				userResult.tasks = data;
				userResult.save(function(err) {
					if (err) {
						res.json({
							message: messagesConfig.savingError,
							error: true,
							data: err
						});
					}
					res.json({
						message: messagesConfig.dataSavedSuccess,
						error: false,
						data: userResult
					});
				});

			});
			res.json({
				message: messagesConfig.dataSavedSuccess,
				data: result
			});
		} else {
			user.save(function(err, savedUser) {
				if (err) {
					res.json({
						message: messagesConfig.savingError,
						error: true,
						data: err
					});
				}
				res.json({
					message: messagesConfig.dataSavedSuccess,
					error: false,
					data: savedUser
				});
			});
		}
	});
};