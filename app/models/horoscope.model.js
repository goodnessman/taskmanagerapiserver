const mongoose = require('mongoose');
const HoroscopeSchema = mongoose.Schema({
	date: String,
	signs: Array
}, {
	timestamps: true
});

module.exports = mongoose.model('HoroscopeModel', HoroscopeSchema);