const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
	tasks: String,
	uuid: String
}, {
	timestamps: true
});

module.exports = mongoose.model('UserModel', UserSchema);