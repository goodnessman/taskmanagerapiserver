const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');
const messagesConfig = require('./config/messages.config.js');
const app = express();

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

mongoose.Promise = global.Promise;

// Allow send requests from all damains
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const port = process.env.PORT || 8080; // set our port

// Connecting to the database
mongoose.connect(dbConfig.url, {
	useNewUrlParser: true
}).then(() => {
	console.log('Successfully connected to the database');
}).catch(err => {
	console.log('Could not connect to the database. Exiting now...', err);
	process.exit();
});

// define a simple route
app.get('/', (req, res) => {
	res.json({
		message: messagesConfig.welcome,
			error: false,
			data: null
		});
});

// create our router
const router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	next();
});

require('./app/routes/user.routes.js')(router);
require('./app/routes/horoscope.routes.js')(router);

// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// listen for requests
app.listen(port, () => {
	console.log('Server is listening on port ' + port);
});
